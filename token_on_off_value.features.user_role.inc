<?php
/**
 * @file
 * token_on_off_value.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function token_on_off_value_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  return $roles;
}
